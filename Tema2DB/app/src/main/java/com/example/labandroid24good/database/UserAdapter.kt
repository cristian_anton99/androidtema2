package com.example.labandroid24good.database

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.labandroid24good.R
import kotlinx.android.synthetic.main.user_row.view.*

class UserAdapter(private val users: List<User?>?) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.user_row,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount() = users!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.view.name1.text= users!![position]!!.firstName+" "+users!![position]!!.lastName
    }

    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view)
}
