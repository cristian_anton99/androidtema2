package com.example.labandroid24good.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity
data class User(
    @PrimaryKey(autoGenerate = true) val uid: Int= 0,
    @SerializedName("name")
    @ColumnInfo(name = "first_name")
    var firstName: String,
    @ColumnInfo(name = "last_name")
    var lastName: String
)


