package com.example.labandroid24good.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.labandroid24good.database.AppDatabase
import com.example.labandroid24good.R
import com.example.labandroid24good.database.User
import com.example.labandroid24good.database.UserAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_activity1.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL


class Activity1Fragment : Fragment() {



    companion object {
        fun newInstance() = Activity1Fragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View=inflater.inflate(R.layout.fragment_activity1, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val db = Room.databaseBuilder(
            activity!!.applicationContext,
            AppDatabase::class.java,
            "users_database"
        )
            .allowMainThreadQueries()
            .build()

        var allUsers = db.userDao().getAllUsers()
        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity!!.applicationContext)
            adapter = UserAdapter(allUsers)

        }

        fun closeKeyboard() {
            val view: View = activity!!.currentFocus!!
            if (view != null) {
                val imm: InputMethodManager =
                    activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        button.setOnClickListener() {
            db.userDao().insertUser(
                User(
                    firstName = firstName.text.toString(),
                    lastName = lastName.text.toString()
                )
            )
            var allUsers = db.userDao().getAllUsers()
            recyclerView.apply {
                layoutManager = LinearLayoutManager(activity!!.applicationContext)
                adapter =
                    UserAdapter(allUsers)

            }
            firstName.text = null
            lastName.text = null
            closeKeyboard()


        }
        button2.setOnClickListener() {
            if (db.userDao().findByName(
                    first = firstName.text.toString(),
                    last = lastName.text.toString()
                ) != null
            ) {
                val foundUser = db.userDao()
                    .findByName(first = firstName.text.toString(), last = lastName.text.toString())
                db.userDao().delete(foundUser)
            } else Toast.makeText(
                activity!!.applicationContext,
                "User couldn`t be found",
                Toast.LENGTH_LONG
            ).show()

            var allUsers = db.userDao().getAllUsers()
            recyclerView.apply {
                layoutManager = LinearLayoutManager(activity!!.applicationContext)
                adapter =
                    UserAdapter(allUsers)

            }
            firstName.text = null
            lastName.text = null
            closeKeyboard()
        }
        button3.setOnClickListener() {
            doAsync {
                val json = URL("https://jsonplaceholder.typicode.com/users").readText()
                uiThread {
                    var users = Gson().fromJson(json, Array<User>::class.java).toList()
                    var i=0
                    while(i<users.size){
                     var stringAux:String =users[i].firstName.toString()
                        if(stringAux.length>3){
                            users[i].lastName=stringAux.substring(stringAux.lastIndexOf(" ")+1)//doar ultimul nume
                            users[i].firstName=stringAux.substring(0,stringAux.lastIndexOf(' '))//toate numele in afara de ultimul
                        }
                        db.userDao().insertUser(users[i])
                        i++
                    }
                    var allUsers = db.userDao().getAllUsers()
                    recyclerView.apply {
                        layoutManager = LinearLayoutManager(activity!!.applicationContext)
                        adapter = UserAdapter(allUsers)
                    }
                }
            }
        }


    }
    }


