package com.example.labandroid24good.database

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.labandroid24good.R

class rview_adapter :RecyclerView.Adapter<rview_adapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val layoutView= LayoutInflater.from(parent.context).inflate(R.layout.lesson_three_fragment,parent,false)
        return ViewHolder(
            layoutView
        )
    }

    override fun getItemCount()=50

    override fun onBindViewHolder(holder: ViewHolder, position: Int){}

    class ViewHolder(val view: View) :RecyclerView.ViewHolder(view)
}